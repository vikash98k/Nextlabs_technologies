from django.urls import path,include
from project.api import views
from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt.views import TokenObtainPairView,TokenRefreshView
from .views import RegisterView

router = DefaultRouter()
router.register('application',views.ApplicationViewSet,basename='application')
router.register('apitask',views.TaskViewSet,basename='apitask')

urlpatterns = [
	path('',include(router.urls)),
    path('api-auth/', include('rest_framework.urls')),
    path('api/register/', RegisterView.as_view(),name='register-view'),
    path('api/login/', TokenObtainPairView.as_view(), name='login-api'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh')
	]
 
