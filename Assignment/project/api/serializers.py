from project.models import Application,Task
from rest_framework import serializers
from django.contrib.auth.models import User

class ApplicationSerializer(serializers.ModelSerializer):
	class Meta:
		model = Application
		fields = ['id','app_name','app_category','sub_category','point'] 

class TaskSerializer(serializers.ModelSerializer):
	application = serializers.CharField()
	user = serializers.CharField()
	class Meta:
		model = Task
		fields = ['id','user','application']

class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(max_length=65, min_length=8, write_only=True)
    class Meta:
        model = User
        fields = ['username','password']

    def create(self, validated_data):
    	return User.objects.create_user(**validated_data)

