from django.core.management.base import BaseCommand
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google.oauth2 import service_account
from project.models import Application

SERVICE_ACCOUNT_FILE = './keys.json'
SCOPES = ['https://www.googleapis.com/auth/spreadsheets']


class Command(BaseCommand):
    help = 'upload sheet data'

    def handle(self, *args, **kwargs):
        creds = service_account.Credentials.from_service_account_file(
            SERVICE_ACCOUNT_FILE, scopes=SCOPES)
        SAMPLE_SPREADSHEET_ID = '14thWjndJCdA2Mwado9I2TJsQg8R4Gez0lejXO8eekX4'
        service = build('sheets', 'v4', credentials=creds)
        sheet = service.spreadsheets()
        result = sheet.values().get(spreadsheetId=SAMPLE_SPREADSHEET_ID,
                                    range="Task!A3:D6").execute()
        values = result.get('values', [])
        for value in values:
            value = Application.objects.create(app_name=value[1],app_category=value[2],point=value[3])
        return value
