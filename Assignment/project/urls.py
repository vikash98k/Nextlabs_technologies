from django.urls import path
from . import views
urlpatterns = [
    path('', views.home, name='home'),
    path('apps/', views.addingapp, name='addingapp'),
    path('app-data/<int:pk>/', views.addingapp_detail, name='app-data'),
    path('task/', views.task, name='task_detail'),
    path('point/', views.point, name='point'),

]
