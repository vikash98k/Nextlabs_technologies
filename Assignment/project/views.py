from django.shortcuts import render, redirect
from .forms import ApplicationForm, TaskForm
from .models import Application, Task
from django.http import HttpResponse, JsonResponse
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

def home(request):
    if request.user.is_authenticated: 
        task_claimed = list(Task.objects.filter(
            user=request.user).values_list('application_id', flat=True))
        data = Application.objects.exclude(id__in=task_claimed)
    else:
        data = Application.objects.all()
    context = {"data": data}
    return render(request, 'project/home.html', context)

@login_required # only authorized user will access this page 
def addingapp(request):
    if request.method == 'POST':
        # request.FILES is used for data upload and store mulitple data in memory
        form = ApplicationForm(request.POST, request.FILES) 
        if form.is_valid():
            form.save()
            return redirect('home')
    else:
        form = ApplicationForm()
    context = {"form": form}
    return render(request, 'project/add_apps.html', context)

@login_required
def addingapp_detail(request, pk):
    app = Application.objects.get(id=pk)
    if request.method == "POST":
        form = TaskForm(request.POST, request.FILES)
        if form.is_valid():
            task = form.save(commit=False)
            task.user = request.user
            task.application = app
            form.save()
            messages.success(request, f'Task Has Been completed')
            return redirect('home')
    else:
        form = TaskForm()
    context = {

        "form": form,
        "app": app
    }
    return render(request, 'project/app_details.html', context)


def task(request):
    # Task.object.filter get only specific information
    tasks = Task.objects.filter(user=request.user)
    context = {'tasks': tasks}
    return render(request, 'project/task.html', context)


def point(request):
    points = 0
    tasks = Task.objects.filter(user=request.user)
    for task in tasks:
        points += task.application.point
    context = {"points": points}
    return render(request, 'project/point.html', context)
