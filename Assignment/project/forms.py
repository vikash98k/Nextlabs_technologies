from django import forms
from .models import Application, Task


class ApplicationForm(forms.ModelForm):
    app_name = forms.CharField(label='App Name', widget=forms.TextInput(
        attrs={'placeholder': 'App Name'}))
    app_link = forms.CharField(label='App Link', widget=forms.TextInput(
        attrs={'placeholder': 'App Link'}))

    class Meta:
        model = Application
        fields = ['app_name', 'app_link',
                  'app_category', 'sub_category', 'photo', 'point']
        labels = {"photo": ''} 


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ['screenshot']
