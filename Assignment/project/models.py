from django.db import models
from django.contrib.auth.models import User
from PIL import Image


class Application(models.Model):
    APP_CHOICES = [
        ('Entertainment', 'Entertainment'),
        ('Educational', 'Educational'),
    ]
    SUB_CHOICES = [
        ('Social Media', 'Social Media'),
        ('Blog', 'Blog')
    ]
    photo = models.ImageField(upload_to='app_image')
    app_name = models.CharField(max_length=100)
    app_link = models.URLField(max_length=50)
    app_category = models.CharField(max_length=20, choices=APP_CHOICES)
    sub_category = models.CharField(max_length=20, choices=SUB_CHOICES)
    point = models.IntegerField()


class Task(models.Model):
    screenshot = models.ImageField(upload_to='images_upload')
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    application = models.ForeignKey(
        Application, on_delete=models.CASCADE, null=True)
