# Nextlabs Assignment
    Live at https://nextlabassignment.herokuapp.com/

## Local Setup
* Git clone:` git clone https://github.com/vikash98k/django_project_.git`
* Go to Project directory: `cd Assignment`
* Create A virtual environment: `virtualenv venv(virtual environment name)`
* Activate Virtual environment: `venv/Scripts/activate`
* Run migration commands: `python manage.py makemigraion,python manage.py migrate`
* Run locally: `python manage.py runserver`

## Task and Solutions
1)  Python write a regex to extract all the numbers with orange color.

    solution: [python_regex](https://gitlab.com/vikash98k/Nextlabs_technologies/-/blob/master/python_regex/regex_solution.py)

2) Use Flask instead of Django and vice versa.

    solution: [django_flask](https://gitlab.com/vikash98k/Nextlabs_technologies/-/blob/master/django_flask/django_flask.txt)

3) API 

    solution : [API](https://gitlab.com/vikash98k/Nextlabs_technologies/-/blob/master/API%20Documentation)

## Sample Video of Task 3

* Sample Video [Here](https://gitlab.com/vikash98k/Nextlabs_technologies/-/blob/master/https___djangoassignuseradmin.herokuapp.com_-_Google_Chrome_2021-08-24_19-26-23.mp4)
